/**
 * Adapted from https://github.com/reduxjs/redux/blob/master/rollup.config.js
 */



import nodeResolve from '@rollup/plugin-node-resolve';
import babel from '@rollup/plugin-babel';
import commonjs from '@rollup/plugin-commonjs'
import includePaths from 'rollup-plugin-includepaths';
import copy from 'rollup-plugin-copy';
import scss from 'rollup-plugin-scss'
import { terser } from "rollup-plugin-terser";
import pkg from './package.json';

import alias from '@rollup/plugin-alias';


let includePathOptions = {
  include: {},
  paths: ['src/'],
  external: [],
  extensions: ['.js', '.json', '.html','.scss']
};



const package_externals = (pkg) => (externals = []) => {
  return externals.concat(Object.keys(pkg.dependencies)).concat(Object.keys(pkg.peerDependencies));
}




const default_external = package_externals(pkg)([]);


const default_plugins = [
  alias({
    entries: [
      { find: 'bia-layout', replacement: './' },
    ]
  }),
  nodeResolve({browser:true}),
  includePaths(includePathOptions),

  babel({ babelHelpers: 'bundled'}),
  commonjs(),
  terser(),
  scss(),
];




const ReactGlobals = {
  react: "React",
  "prop-types": "PropTypes",
};


const defaultConf = {
  external:default_external,
  plugins:default_plugins
}
console.log(default_external)


const defaultOutputOptions = {
  format: 'cjs',
  indent: false,
  sourcemap: false,
  exports: 'named',
}


const make_pkg = (format,moreOutputOptions={})=> (input, output,options={}, more_plugins=[]) => {
  return Object.assign(
    {},
    defaultConf,
    {
      input,
      output: Object.assign({}, defaultOutputOptions, { file: output,format, ...moreOutputOptions,...options }),
      plugins: [...default_plugins, ...more_plugins],
    
    }
  );
};

const make_umd = make_pkg('umd',{globals:ReactGlobals});

const make_cjs = make_pkg('cjs',{});

const make_es = make_pkg('es',{});


const copy_package_json = target => copy({
  targets: [
    { src: 'package.json', dest: target }
  ]
});



export default [

  make_cjs('src/index.js', pkg.main),
  make_umd('src/index.js', pkg.browser,{name:'ReactCoreLayout'}),
  make_es('src/index.js', pkg.module),

]
